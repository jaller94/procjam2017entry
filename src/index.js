const imagePaths = [];
const images = [];

function addTile(id, path) {
  const myIcon = new Image();
  myIcon.src = path;
  imagePaths[id] = path;
  images[id] = myIcon;
}

import img67 from './images/landscapeTiles_067.png';
addTile(67, img67);
import img41 from './images/landscapeTiles_041.png';
addTile(41, img41);
import img27 from './images/landscapeTiles_027.png';
addTile(27, img27);
import img49 from './images/landscapeTiles_049.png';
addTile(49, img49);
import img34 from './images/landscapeTiles_034.png';
addTile(34, img34);
import img66 from './images/landscapeTiles_066.png';
addTile(66, img66);
import img35 from './images/landscapeTiles_035.png';
addTile(35, img35);
import img48 from './images/landscapeTiles_048.png';
addTile(48, img48);
import img42 from './images/landscapeTiles_042.png';
addTile(42, img42);
import img56 from './images/landscapeTiles_056.png';
addTile(56, img56);

const riot = `67 67 67 67 67
67 41 27 49 67
67 34 66 35 67
67 48 42 56 67
67 67 67 67 67`.split('\n').map((row) => row.split(' '));

function createMapBlock() {
  const block = [];
  for (let y = 0; y < 8; y++) {
    const row = [];
    for (let x = 0; x < 8; x++) {
      const stack = [];
      for (let z = 0; z < 8; z++) {
        const items = [0,0,0,0,0,67,41,27,49];
        const randItem = items[Math.floor(Math.random()*items.length)];
        //stack.push(randItem);
        if (z === 0) {
          stack.push(67);
        } else {
          stack.push(0);
        }
      }
      row.push(stack);
    }
    block.push(row);
  }
  return block;
}

function create2DMask(width, height) {
  const block = [];
  for (let y = 0; y < width; y++) {
    const row = [];
    for (let x = 0; x < height; x++) {
      row.push(0);
    }
    block.push(row);
  }
  return block;
}

const lakeRules = [
  ['0111', 34],
  ['1011', 42],
  ['1101', 35],
  ['1110', 27],
  ['0011', 48],
  ['1001', 56],
  ['1100', 49],
  ['0110', 41],
  ['1111', 66],
];

function pick4Direction(mask, x, y) {
  for(const rule of lakeRules) {
    if (y === 0 || mask[x][y-1] != rule[0].charAt(0)) continue;
    if (x === mask[0].length - 1 || mask[x+1][y] != rule[0].charAt(1)) continue;
    if (y === mask.length - 1 || mask[x][y+1] != rule[0].charAt(2)) continue;
    if (x === 0 || mask[x-1][y] != rule[0].charAt(3)) continue;
      return rule[1];
  }
  if (mask[x][y] === 1) {
    return 66;
  }
}

function apply2DLake(world, mask, xOffset, yOffset, zOffset) {
  for (let y = 0; y < 8; y++) {
    for (let x = 0; x < 8; x++) {
      const tile = pick4Direction(mask, x, y);
      if (tile) {
        world[xOffset+x][yOffset+y][zOffset] = tile;
      }
    }
  }
}

const map = createMapBlock();
const mask = create2DMask(8, 8);
mask[3][3] = 1;
mask[4][3] = 1;
mask[3][4] = 1;
mask[4][4] = 1;
mask[2][3] = 1;
mask[2][4] = 1;
mask[2][2] = 1;
mask[3][2] = 1;
mask[4][2] = 1;
apply2DLake(map, mask, 0, 0, 0);

const canvas = document.getElementById('main');
const ctx = canvas.getContext('2d');

let xOffset = 150;
let yOffset = 150;

function drawAt(ctx, img, x, y, z) {
  const xPix = xOffset + x * 65 + y * -65;
  const yPix = yOffset + x * 32 + y * 32 - z * 32 - img.height;

  ctx.drawImage(img, xPix, yPix);
  //ctx.fillText(x + ', ' + y, xPix + 56, yPix + 30);
}

function resizeCanvas() {
  canvas.width = window.innerWidth;
  canvas.height = window.innerHeight;
}

function redraw() {
  ctx.clearRect(0, 0, canvas.width, canvas.height);
  for (let y = 0; y < map.length; y++) {
    for (let x = 0; x < map[0].length; x++) {
      for (let z = 0; z < map[0][0].length; z++) {
        const tileId = map[y][x][z];
        if (tileId !== 0) {
          drawAt(ctx, images[tileId], x, y, z);
        }
      }
    }
  }
  window.requestAnimationFrame(redraw);
}

resizeCanvas();
redraw();

window.addEventListener('resize', function (event) {
  resizeCanvas();
});

let isDragging = false;
let downX = 0;
let downY = 0;

function startDragging(e) {
  e.preventDefault();
  e.stopPropagation();
  isDragging = true;
  downX = e.clientX;
  downY = e.clientY;
  window.addEventListener('mousemove', dragging);
  window.addEventListener('mouseup', stopDragging);
}

function dragging(e) {
  e.preventDefault();
  e.stopPropagation();
  if (isDragging) {
    xOffset += e.clientX - downX;
    downX = e.clientX;
    yOffset += e.clientY - downY;
    downY = e.clientY;
  }
}

function stopDragging(e) {
  e.preventDefault();
  e.stopPropagation();
  isDragging = false;
  window.removeEventListener('mousemove', dragging);
  window.removeEventListener('mouseup', stopDragging);
}

window.addEventListener('mousedown', startDragging);
/*
window.addEventListener('touchdown', startDragging);
window.addEventListener('touchup', stopDragging);
window.addEventListener('touchmove', dragging);
*/
